#!/bin/sh -e

curl -i -H "Content-Type: application/json" http://localhost:8080/pedido -X POST -d '{"cliente":"joao", "endereco":"r das casas, 0", "idPizza":1, "idMassa":1, "idBorda":1, "idTamanho":1, "quantidade":1}'

curl -i -H "Content-Type: application/json" http://localhost:8080/pedido/1 -X PUT -d '{"cliente":"joao", "endereco":"r das casas, 0", "idPizza":1, "idMassa":2, "idBorda":2, "idTamanho":3, "quantidade":1}'

curl -i -H "Content-Type: application/json" http://localhost:8080/pedido/1/cobertura/7 -X POST

curl -i -H "Content-Type: application/json" http://localhost:8080/pedido/1/cobertura/8 -X DELETE

curl -i -H "Content-Type: application/json" http://localhost:8080/tamanho -X POST -d '{"nome":"Gigante", "pedacos":12}'

curl -i -H "Content-Type: application/json" http://localhost:8080/pedido/1 -X PATCH -d '[{"op":"replace", "path":"/massa", "value":2}, {"op":"replace", "path":"/tamanho", "value":5}, {"op":"replace", "path":"/borda", "value":3}, {"op":"replace", "path":"/tamanho", "value":4}, {"op":"replace", "path":"/quantidade", "value":2}]'

curl -i -H "Content-Type: application/json" http://localhost:8080/borda -X OPTIONS