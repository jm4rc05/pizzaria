package net.allegea.pizzaiolo.support;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import net.allegea.pizzaiolo.model.Pedido;
import net.allegea.pizzaiolo.service.PedidoController;

@Component
public class PedidoResourceAssembler extends ResourceAssemblerSupport<Pedido, PedidoResource> {

    public PedidoResourceAssembler() {
        super(PedidoController.class, PedidoResource.class);
    }

    public PedidoResource toResource(Pedido pedido) {
        PedidoResource resource = createResourceWithId(pedido.getId(), pedido);
        resource.add(linkTo(methodOn(PedidoController.class).massa(pedido.getId())).withRel(LinkRelationName.PEDIDO_MASSA));
        resource.add(linkTo(methodOn(PedidoController.class).borda(pedido.getId())).withRel(LinkRelationName.PEDIDO_BORDA));
        resource.add(linkTo(methodOn(PedidoController.class).coberturas(pedido.getId())).withRel(LinkRelationName.PEDIDO_COBERTURA));
        resource.add(linkTo(methodOn(PedidoController.class).tamanho(pedido.getId())).withRel(LinkRelationName.PEDIDO_TAMANHO));

        return resource;
    }

    @Override
    protected PedidoResource instantiateResource(Pedido pedido) {
        return new PedidoResource(pedido);
    }

}
