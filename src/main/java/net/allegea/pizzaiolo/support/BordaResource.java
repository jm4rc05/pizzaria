package net.allegea.pizzaiolo.support;

import org.springframework.hateoas.ResourceSupport;

import net.allegea.pizzaiolo.model.Borda;

public class BordaResource extends ResourceSupport {

    private Borda borda;

    public BordaResource(Borda borda) {
        super();

        this.borda = borda;
    }

    public Borda getBorda() {
        return borda;
    }

}
