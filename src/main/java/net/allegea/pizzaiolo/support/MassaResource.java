package net.allegea.pizzaiolo.support;

import org.springframework.hateoas.ResourceSupport;

import net.allegea.pizzaiolo.model.Massa;

public class MassaResource extends ResourceSupport {

    private Massa massa;

    public MassaResource(Massa massa) {
        super();

        this.massa = massa;
    }

    public Massa getMassa() {
        return massa;
    }

}
