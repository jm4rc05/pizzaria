package net.allegea.pizzaiolo.support;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import net.allegea.pizzaiolo.model.Cobertura;
import net.allegea.pizzaiolo.service.CardapioController;
import net.allegea.pizzaiolo.service.CoberturaController;
import net.allegea.pizzaiolo.service.PedidoController;

@Component
public class CoberturaResourceAssembler extends ResourceAssemblerSupport<Cobertura, CoberturaResource> {

    public CoberturaResourceAssembler() {
        super(CoberturaController.class, CoberturaResource.class);
    }

    public CoberturaResource toResource(Cobertura cobertura) {
        CoberturaResource resource = createResourceWithId(cobertura.getId(), cobertura);

        return resource;
    }

    public List<CoberturaResource> toResourcesForPedido(int id, List<Cobertura> coberturas) {
        List<CoberturaResource> list = new ArrayList<CoberturaResource>();

        for (Cobertura cobertura : coberturas) {
            CoberturaResource resource = instantiateResource(cobertura);
            resource.add(linkTo(methodOn(PedidoController.class).cobertura(id, cobertura.getId())).withRel(LinkRelationName.SELF));
            resource.add(linkTo(methodOn(PedidoController.class).pedido(id)).withRel(LinkRelationName.PEDIDO));
            resource.add(linkTo(methodOn(CoberturaController.class).cobertura(cobertura.getId())).withRel(LinkRelationName.COBERTURA));

            list.add(resource);
        }

        return list;
    }

    public List<CoberturaResource> toResourcesForCardapio(int id, List<Cobertura> coberturas) {
        List<CoberturaResource> list = new ArrayList<CoberturaResource>();

        for (Cobertura cobertura : coberturas) {
            CoberturaResource resource = instantiateResource(cobertura);
            resource.add(linkTo(methodOn(CardapioController.class).cobertura(id, cobertura.getId())).withRel(LinkRelationName.SELF));
            resource.add(linkTo(methodOn(CardapioController.class).cardapio(id)).withRel(LinkRelationName.CARDAPIO));
            resource.add(linkTo(methodOn(CoberturaController.class).cobertura(cobertura.getId())).withRel(LinkRelationName.COBERTURA));

            list.add(resource);
        }

        return list;
    }

    @Override
    protected CoberturaResource instantiateResource(Cobertura cobertura) {
        return new CoberturaResource(cobertura);
    }

}
