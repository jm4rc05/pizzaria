package net.allegea.pizzaiolo.support;

import org.springframework.hateoas.ResourceSupport;

import net.allegea.pizzaiolo.model.Cobertura;

public class CoberturaResource extends ResourceSupport {

    private Cobertura cobertura;

    public CoberturaResource(Cobertura cobertura) {
        super();

        this.cobertura = cobertura;
    }

    public Cobertura getCobertura() {
        return cobertura;
    }

}
