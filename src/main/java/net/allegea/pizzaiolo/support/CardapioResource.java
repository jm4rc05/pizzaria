package net.allegea.pizzaiolo.support;

import org.springframework.hateoas.ResourceSupport;

import net.allegea.pizzaiolo.model.Cardapio;

public class CardapioResource extends ResourceSupport {

    private Cardapio cardapio;

    public CardapioResource(Cardapio cardapio) {
        super();

        this.cardapio = cardapio;
    }

    public Cardapio getCardapio() {
        return cardapio;
    }

}
