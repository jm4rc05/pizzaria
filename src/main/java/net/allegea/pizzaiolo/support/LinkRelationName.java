package net.allegea.pizzaiolo.support;

public interface LinkRelationName {

    String SELF = "self";

    String PEDIDO = "pedido";

    String MASSA = "massa";

    String BORDA = "borda";

    String COBERTURA = "cobertura";

    String TAMANHO = "tamanho";

    String CARDAPIO = "cardapio";

    String PEDIDO_MASSA = "pedido/massa";

    String PEDIDO_BORDA = "pedido/borda";

    String PEDIDO_COBERTURA = "pedido/cobertura";

    String PEDIDO_TAMANHO = "pedido/tamanho";

}
