package net.allegea.pizzaiolo.support;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import net.allegea.pizzaiolo.model.Option;
import net.allegea.pizzaiolo.service.OptionController;


public class OptionResourceAssembler extends ResourceAssemblerSupport<Option, OptionResource> {

    public OptionResourceAssembler() {
        super(OptionController.class, OptionResource.class);
    }
    
    @Override
    public OptionResource toResource(Option option) {
        OptionResource resource = createResourceWithId(-1, option);
        
        return resource;
    }
    
    @Override
    protected OptionResource instantiateResource(Option option) {
        return new OptionResource(option);
    }

}
