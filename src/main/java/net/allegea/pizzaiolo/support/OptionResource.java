package net.allegea.pizzaiolo.support;

import org.springframework.hateoas.ResourceSupport;

import net.allegea.pizzaiolo.model.Option;

public class OptionResource extends ResourceSupport {
    
    private Option option;
    
    public OptionResource(Option option) {
        super();
        
        this.option = option;
    }
    
    public Option getOption() {
        return option;
    }

}
