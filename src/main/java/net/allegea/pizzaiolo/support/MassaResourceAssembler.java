package net.allegea.pizzaiolo.support;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import net.allegea.pizzaiolo.model.Massa;
import net.allegea.pizzaiolo.service.MassaController;

@Component
public class MassaResourceAssembler extends ResourceAssemblerSupport<Massa, MassaResource> {

    public MassaResourceAssembler() {
        super(MassaController.class, MassaResource.class);
    }

    public MassaResource toResource(Massa massa) {
        MassaResource resource = createResourceWithId(massa.getId(), massa);

        return resource;
    }

    @Override
    protected MassaResource instantiateResource(Massa massa) {
        return new MassaResource(massa);
    }

}
