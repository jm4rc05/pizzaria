package net.allegea.pizzaiolo.support;

import org.springframework.hateoas.ResourceSupport;

import net.allegea.pizzaiolo.model.Pedido;

public class PedidoResource extends ResourceSupport {

    private Pedido pedido;

    public PedidoResource(Pedido pedido) {
        super();

        this.pedido = pedido;
    }

    public Pedido getPedido() {
        return pedido;
    }

}
