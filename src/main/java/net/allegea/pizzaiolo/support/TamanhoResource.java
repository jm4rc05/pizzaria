package net.allegea.pizzaiolo.support;

import org.springframework.hateoas.ResourceSupport;

import net.allegea.pizzaiolo.model.Tamanho;

public class TamanhoResource extends ResourceSupport {

    private Tamanho tamanho;

    public TamanhoResource(Tamanho tamanho) {
        super();

        this.tamanho = tamanho;
    }

    public Tamanho getTamanho() {
        return tamanho;
    }

}
