package net.allegea.pizzaiolo.support;

import org.springframework.hateoas.ResourceSupport;

import net.allegea.pizzaiolo.model.Message;

public class MessageResource extends ResourceSupport {

    private Message message;

    public MessageResource(Message message) {
        super();

        this.message = message;
    }

    public Message getMessage() {
        return message;
    }

}
