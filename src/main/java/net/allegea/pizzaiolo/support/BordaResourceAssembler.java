package net.allegea.pizzaiolo.support;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import net.allegea.pizzaiolo.model.Borda;
import net.allegea.pizzaiolo.service.BordaController;

@Component
public class BordaResourceAssembler extends ResourceAssemblerSupport<Borda, BordaResource> {

    public BordaResourceAssembler() {
        super(BordaController.class, BordaResource.class);
    }

    public BordaResource toResource(Borda borda) {
        BordaResource resource = createResourceWithId(borda.getId(), borda);

        return resource;
    }

    @Override
    protected BordaResource instantiateResource(Borda borda) {
        return new BordaResource(borda);
    }

}
