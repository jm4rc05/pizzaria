package net.allegea.pizzaiolo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.hateoas.config.EnableHypermediaSupport.HypermediaType;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import net.allegea.pizzaiolo.model.Cobertura;
import net.allegea.pizzaiolo.model.PizzariaService;
import net.allegea.pizzaiolo.support.CoberturaResource;
import net.allegea.pizzaiolo.support.CoberturaResourceAssembler;
import net.allegea.pizzaiolo.support.MessageResource;
import net.allegea.pizzaiolo.support.MessageResourceAssembler;

@RestController
@ExposesResourceFor(Cobertura.class)
@EnableHypermediaSupport(type = { HypermediaType.HAL })
@RequestMapping("/cobertura")
public class CoberturaController {

    public interface Request {

        String ID = "/{id}";

    }

    @Autowired
    private CoberturaResourceAssembler resourceAssembler;

    @Autowired
    private MessageResourceAssembler messageResourceAssembler;

    @Autowired
    private PizzariaService service;

    @RequestMapping(method = RequestMethod.OPTIONS, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<MessageResource> options(@RequestHeader(value = "Version", required = false) String version) {
        MessageResource resource = messageResourceAssembler.toResource(2, "Not implemented");

        return new ResponseEntity<MessageResource>(resource, HttpStatus.NOT_IMPLEMENTED);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<CoberturaResource> adicionar(@RequestBody Cobertura body) {
        Cobertura cobertura = service.adicionarCobertura(body);
        CoberturaResource resource = resourceAssembler.toResource(cobertura);

        return new ResponseEntity<CoberturaResource>(resource, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE }, value = Request.ID)
    public ResponseEntity<CoberturaResource> atualizar(@PathVariable("id") int id, @RequestBody Cobertura body) {
        Cobertura cobertura = service.atualizarCobertura(id, body);
        CoberturaResource resource = resourceAssembler.toResource(cobertura);

        return new ResponseEntity<CoberturaResource>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<CoberturaResource>> coberturas(@RequestHeader(value = "Version", required = false) String version) {
        List<Cobertura> coberturas = service.obterCoberturas();
        List<CoberturaResource> resource = resourceAssembler.toResources(coberturas);

        return new ResponseEntity<List<CoberturaResource>>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, value = Request.ID)
    public ResponseEntity<CoberturaResource> cobertura(@PathVariable("id") int id) {
        Cobertura cobertura = service.obterCobertura(id);
        CoberturaResource resource = resourceAssembler.toResource(cobertura);

        return new ResponseEntity<CoberturaResource>(resource, HttpStatus.OK);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseEntity<MessageResource> handleExceptions(Exception ex) {
        MessageResource resource = messageResourceAssembler.toResource(2, ex.getMessage());

        return new ResponseEntity<MessageResource>(resource, HttpStatus.BAD_REQUEST);
    }

}
