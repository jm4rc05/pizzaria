package net.allegea.pizzaiolo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.hateoas.config.EnableHypermediaSupport.HypermediaType;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import net.allegea.pizzaiolo.model.Massa;
import net.allegea.pizzaiolo.model.PizzariaService;
import net.allegea.pizzaiolo.support.MassaResource;
import net.allegea.pizzaiolo.support.MassaResourceAssembler;
import net.allegea.pizzaiolo.support.MessageResource;
import net.allegea.pizzaiolo.support.MessageResourceAssembler;

@RestController
@ExposesResourceFor(Massa.class)
@EnableHypermediaSupport(type = { HypermediaType.HAL })
@RequestMapping("/massa")
public class MassaController {

    public interface Request {

        String ID = "/{id}";

    }

    @Autowired
    private MassaResourceAssembler resourceAssembler;

    @Autowired
    private MessageResourceAssembler messageResourceAssembler;

    @Autowired
    private PizzariaService service;

    @RequestMapping(method = RequestMethod.OPTIONS, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<MessageResource> options(@RequestHeader(value = "Version", required = false) String version) {
        MessageResource resource = messageResourceAssembler.toResource(2, "Not implemented");

        return new ResponseEntity<MessageResource>(resource, HttpStatus.NOT_IMPLEMENTED);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<MassaResource> adicionar(@RequestBody Massa body) {
        Massa massa = service.adicionarMassa(body);
        MassaResource resource = resourceAssembler.toResource(massa);

        return new ResponseEntity<MassaResource>(resource, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE }, value = Request.ID)
    public ResponseEntity<MassaResource> atualizar(@PathVariable("id") int id, @RequestBody Massa body) {
        Massa massa = service.atualizarMassa(id, body);
        MassaResource resource = resourceAssembler.toResource(massa);

        return new ResponseEntity<MassaResource>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<MassaResource>> massas(@RequestHeader(value = "Version", required = false) String version) {
        List<Massa> massas = service.obterMassas();
        List<MassaResource> resource = resourceAssembler.toResources(massas);

        return new ResponseEntity<List<MassaResource>>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, value = Request.ID)
    public ResponseEntity<MassaResource> massa(@PathVariable("id") int id) {
        Massa massa = service.obterMassa(id);
        MassaResource resource = resourceAssembler.toResource(massa);

        return new ResponseEntity<MassaResource>(resource, HttpStatus.OK);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseEntity<MessageResource> handleExceptions(Exception ex) {
        MessageResource resource = messageResourceAssembler.toResource(2, ex.getMessage());

        return new ResponseEntity<MessageResource>(resource, HttpStatus.BAD_REQUEST);
    }

}
