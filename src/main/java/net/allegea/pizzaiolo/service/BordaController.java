package net.allegea.pizzaiolo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.hateoas.config.EnableHypermediaSupport.HypermediaType;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import net.allegea.pizzaiolo.model.Borda;
import net.allegea.pizzaiolo.model.Option;
import net.allegea.pizzaiolo.model.OptionParameter;
import net.allegea.pizzaiolo.model.PizzariaService;
import net.allegea.pizzaiolo.support.BordaResource;
import net.allegea.pizzaiolo.support.BordaResourceAssembler;
import net.allegea.pizzaiolo.support.MessageResource;
import net.allegea.pizzaiolo.support.MessageResourceAssembler;
import net.allegea.pizzaiolo.support.OptionResource;
import net.eusashead.hateoas.response.OptionsResponseBuilder;

@RestController
@ExposesResourceFor(Borda.class)
@EnableHypermediaSupport(type = { HypermediaType.HAL })
@RequestMapping("/borda")
public class BordaController {

    public interface Request {

        String ID = "/{id}";

    }

    @Autowired
    private BordaResourceAssembler resourceAssembler;

    @Autowired
    private MessageResourceAssembler messageResourceAssembler;

    @Autowired
    private PizzariaService service;

    @RequestMapping(method = RequestMethod.OPTIONS, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<OptionResource> options(OptionsResponseBuilder<OptionResource> builder) {
        List<OptionParameter> parameters = new ArrayList<OptionParameter>();
        parameters.add(new OptionParameter("nome", "Nome da borda", "String", true));
        parameters.add(new OptionParameter("valor", "Valor da borda", "Number", true));
        Option option = new Option("POST", "Grava borda", parameters);
        return builder
            .entity(new OptionResource(option)) 
            .allow(HttpMethod.GET, HttpMethod.POST, HttpMethod.PUT)
            .build();
    }
    
    @RequestMapping(method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<BordaResource> adicionar(@RequestBody Borda body) {
        Borda borda = service.adicionarBorda(body);
        BordaResource resource = resourceAssembler.toResource(borda);

        return new ResponseEntity<BordaResource>(resource, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE }, value = Request.ID)
    public ResponseEntity<BordaResource> atualizar(@PathVariable("id") int id, @RequestBody Borda body) {
        Borda borda = service.atualizarBorda(id, body);
        BordaResource resource = resourceAssembler.toResource(borda);

        return new ResponseEntity<BordaResource>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<BordaResource>> bordas() {
        List<Borda> bordas = service.obterBordas();
        List<BordaResource> resource = resourceAssembler.toResources(bordas);

        return new ResponseEntity<List<BordaResource>>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, value = Request.ID)
    public ResponseEntity<BordaResource> borda(@PathVariable("id") int id) {
        Borda borda = service.obterBorda(id);
        BordaResource resource = resourceAssembler.toResource(borda);

        return new ResponseEntity<BordaResource>(resource, HttpStatus.OK);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseEntity<MessageResource> handleExceptions(Exception ex) {
        MessageResource resource = messageResourceAssembler.toResource(2, ex.getMessage());

        return new ResponseEntity<MessageResource>(resource, HttpStatus.BAD_REQUEST);
    }

}
