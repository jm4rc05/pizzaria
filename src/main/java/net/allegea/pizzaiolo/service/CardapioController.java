package net.allegea.pizzaiolo.service;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.hateoas.config.EnableHypermediaSupport.HypermediaType;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import net.allegea.pizzaiolo.model.Cardapio;
import net.allegea.pizzaiolo.model.Cobertura;
import net.allegea.pizzaiolo.model.PizzariaService;
import net.allegea.pizzaiolo.support.CardapioResource;
import net.allegea.pizzaiolo.support.CardapioResourceAssembler;
import net.allegea.pizzaiolo.support.CoberturaResource;
import net.allegea.pizzaiolo.support.CoberturaResourceAssembler;
import net.allegea.pizzaiolo.support.LinkRelationName;
import net.allegea.pizzaiolo.support.MessageResource;
import net.allegea.pizzaiolo.support.MessageResourceAssembler;

@RestController
@ExposesResourceFor(Cardapio.class)
@EnableHypermediaSupport(type = { HypermediaType.HAL })
@RequestMapping("/cardapio")
public class CardapioController {

    public interface Request {

        String ID = "/{id}";

        String COBERTURAS = "/{id}/coberturas";

        String COBERTURA = "/{id}/cobertura/{idCobertura}";

    }

    @Autowired
    private CardapioResourceAssembler resourceAssembler;

    @Autowired
    private CoberturaResourceAssembler coberturaResourceAssembler;

    @Autowired
    private MessageResourceAssembler messageResourceAssembler;

    @Autowired
    private PizzariaService service;

    @RequestMapping(method = RequestMethod.OPTIONS, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<MessageResource> options(@RequestHeader(value = "Version", required = false) String version) {
        MessageResource resource = messageResourceAssembler.toResource(2, "Not implemented");

        return new ResponseEntity<MessageResource>(resource, HttpStatus.NOT_IMPLEMENTED);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<CardapioResource> adicionar(@RequestBody Cardapio body) {
        Cardapio cardapio = service.adicionarCardapio(body);
        CardapioResource resource = resourceAssembler.toResource(cardapio);

        return new ResponseEntity<CardapioResource>(resource, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE }, value = Request.ID)
    public ResponseEntity<CardapioResource> atualizar(@PathVariable("id") int id, @RequestBody Cardapio body) {
        Cardapio cardapio = service.atualizarCardapio(id, body);
        CardapioResource resource = resourceAssembler.toResource(cardapio);

        return new ResponseEntity<CardapioResource>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<CardapioResource>> cardapios(@RequestHeader(value = "Version", required = false) String version) {
        List<Cardapio> cardapios = service.obterCardapios();
        List<CardapioResource> resource = resourceAssembler.toResources(cardapios);

        return new ResponseEntity<List<CardapioResource>>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, value = Request.ID)
    public ResponseEntity<CardapioResource> cardapio(@PathVariable("id") int id) {
        Cardapio cardapio = service.obterCardapio(id);
        CardapioResource resource = resourceAssembler.toResource(cardapio);

        return new ResponseEntity<CardapioResource>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, value = Request.COBERTURAS)
    public ResponseEntity<List<CoberturaResource>> coberturas(@PathVariable("id") int id) {
        Cardapio cardapio = service.obterCardapio(id);
        List<Cobertura> coberturas = cardapio.getCoberturas();
        List<CoberturaResource> resource = coberturaResourceAssembler.toResourcesForCardapio(id, coberturas);

        return new ResponseEntity<List<CoberturaResource>>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, value = Request.COBERTURA)
    public ResponseEntity<CoberturaResource> cobertura(@PathVariable("id") int id, @PathVariable("idCobertura") int idCobertura) {
        Cobertura cobertura = service.obterCoberturaCardapio(id, idCobertura);
        CoberturaResource resource = coberturaResourceAssembler.toResource(cobertura);
        resource.add(linkTo(methodOn(CardapioController.class).cobertura(id, cobertura.getId())).withRel(LinkRelationName.PEDIDO_COBERTURA));
        resource.add(linkTo(methodOn(CardapioController.class).cardapio(id)).withRel(LinkRelationName.PEDIDO));

        return new ResponseEntity<CoberturaResource>(resource, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.POST, value = Request.COBERTURA)
    public ResponseEntity<CardapioResource> coberturaAdicionar(@PathVariable("id") int id, @PathVariable("idCobertura") int idCobertura) {
        Cardapio cardapio = service.adicionarCoberturaCardapio(id, idCobertura);
        CardapioResource resource = resourceAssembler.toResource(cardapio);

        return new ResponseEntity<CardapioResource>(resource, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.DELETE, produces = { MediaType.APPLICATION_JSON_VALUE }, value = Request.COBERTURA)
    public ResponseEntity<CardapioResource> coberturaRemover(@PathVariable("id") int id, @PathVariable("idCobertura") int idCobertura) {
        Cardapio cardapio = service.removerCoberturaCardapio(id, idCobertura);
        CardapioResource resource = resourceAssembler.toResource(cardapio);

        return new ResponseEntity<CardapioResource>(resource, HttpStatus.OK);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseEntity<MessageResource> handleExceptions(Exception ex) {
        MessageResource resource = messageResourceAssembler.toResource(2, ex.getMessage());

        return new ResponseEntity<MessageResource>(resource, HttpStatus.BAD_REQUEST);
    }

}
