package net.allegea.pizzaiolo.service;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.hateoas.config.EnableHypermediaSupport.HypermediaType;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import net.allegea.pizzaiolo.model.Borda;
import net.allegea.pizzaiolo.model.Cobertura;
import net.allegea.pizzaiolo.model.Massa;
import net.allegea.pizzaiolo.model.Patch;
import net.allegea.pizzaiolo.model.Pedido;
import net.allegea.pizzaiolo.model.PedidoRequest;
import net.allegea.pizzaiolo.model.PizzariaService;
import net.allegea.pizzaiolo.model.Tamanho;
import net.allegea.pizzaiolo.support.BordaResource;
import net.allegea.pizzaiolo.support.BordaResourceAssembler;
import net.allegea.pizzaiolo.support.CoberturaResource;
import net.allegea.pizzaiolo.support.CoberturaResourceAssembler;
import net.allegea.pizzaiolo.support.LinkRelationName;
import net.allegea.pizzaiolo.support.MassaResource;
import net.allegea.pizzaiolo.support.MassaResourceAssembler;
import net.allegea.pizzaiolo.support.MessageResource;
import net.allegea.pizzaiolo.support.MessageResourceAssembler;
import net.allegea.pizzaiolo.support.PedidoResource;
import net.allegea.pizzaiolo.support.PedidoResourceAssembler;
import net.allegea.pizzaiolo.support.TamanhoResource;
import net.allegea.pizzaiolo.support.TamanhoResourceAssembler;

@RestController
@ExposesResourceFor(Pedido.class)
@EnableHypermediaSupport(type = { HypermediaType.HAL })
@RequestMapping("/pedido")
public class PedidoController {

    public interface Request {

        String ID = "/{id}";

        String MASSA = "/{id}/massa";

        String MASSA_ID = "/{id}/massa/{idMassa}";

        String BORDA = "/{id}/borda";

        String BORDA_ID = "/{id}/borda/{idBorda}";

        String COBERTURA = "/{id}/cobertura";

        String COBERTURA_ID = "/{id}/cobertura/{idCobertura}";

        String TAMANHO = "/{id}/tamanho";

        String TAMANHO_ID = "/{id}/tamanho/{idTamanho}";

    }

    public interface PatchPath {

        String MASSA = "/massa";

        String BORDA = "/borda";

        String COBERTURA = "/cobertura";

        String TAMANHO = "/tamanho";

        String QUANTIDADE = "/quantidade";

    }

    @Autowired
    private PedidoResourceAssembler resourceAssembler;

    @Autowired
    private MassaResourceAssembler massaResourceAssembler;

    @Autowired
    private BordaResourceAssembler bordaResourceAssembler;

    @Autowired
    private CoberturaResourceAssembler coberturaResourceAssembler;

    @Autowired
    private TamanhoResourceAssembler tamanhoResourceAssembler;

    @Autowired
    private MessageResourceAssembler messageResourceAssembler;

    @Autowired
    private PizzariaService service;

    @RequestMapping(method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<PedidoResource> adicionar(@RequestBody PedidoRequest body) {
        Pedido pedido = service.adicionarPedido(body);
        PedidoResource resource = resourceAssembler.toResource(pedido);

        return new ResponseEntity<PedidoResource>(resource, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE }, value = Request.ID)
    public ResponseEntity<PedidoResource> atualizar(@PathVariable("id") int id, @RequestBody PedidoRequest body) {
        Pedido pedido = service.atualizarPedido(id, body);
        PedidoResource resource = resourceAssembler.toResource(pedido);

        return new ResponseEntity<PedidoResource>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, produces = { MediaType.APPLICATION_JSON_VALUE }, value = Request.ID)
    public ResponseEntity<PedidoResource> remover(@PathVariable("id") int id, @RequestBody PedidoRequest body) {
        Pedido pedido = service.removerPedido(id);
        PedidoResource resource = resourceAssembler.toResource(pedido);

        return new ResponseEntity<PedidoResource>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<PedidoResource>> pedidos(@RequestHeader(value = "Version", required = false) String version) {
        List<Pedido> pedidos = service.obterPedidos();
        List<PedidoResource> resource = resourceAssembler.toResources(pedidos);

        return new ResponseEntity<List<PedidoResource>>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, value = Request.ID)
    public ResponseEntity<PedidoResource> pedido(@PathVariable("id") int id) {
        Pedido pedido = service.obterPedido(id);
        PedidoResource resource = resourceAssembler.toResource(pedido);

        return new ResponseEntity<PedidoResource>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, value = Request.MASSA)
    public ResponseEntity<MassaResource> massa(@PathVariable("id") int id) {
        Pedido pedido = service.obterPedido(id);
        Massa massa = pedido.getMassa();
        MassaResource resource = massaResourceAssembler.toResource(massa);
        resource.add(linkTo(methodOn(PedidoController.class).massa(id, massa.getId())).withRel(LinkRelationName.PEDIDO_MASSA));
        resource.add(linkTo(methodOn(PedidoController.class).pedido(id)).withRel(LinkRelationName.PEDIDO));

        return new ResponseEntity<MassaResource>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, value = Request.MASSA_ID)
    public ResponseEntity<MassaResource> massa(@PathVariable("id") int id, @PathVariable("idMassa") int idMassa) {
        Massa massa = service.obterPedido(id).getMassa();
        MassaResource resource = massaResourceAssembler.toResource(massa);
        resource.add(linkTo(methodOn(PedidoController.class).massa(id, massa.getId())).withRel(LinkRelationName.PEDIDO_MASSA));
        resource.add(linkTo(methodOn(PedidoController.class).pedido(id)).withRel(LinkRelationName.PEDIDO));

        return new ResponseEntity<MassaResource>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PATCH, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE }, value = Request.ID)
    public ResponseEntity<PedidoResource> pedidoAtualizar(@PathVariable("id") int id, @RequestBody ArrayList<Patch> bodyList) {
        for (Patch body : bodyList) {
            if (body.getPath().equals(PatchPath.MASSA)) {
                if (body.getOp().equals(Patch.Operation.REPLACE)) {
                    service.atualizarMassaPedido(id, new Integer(body.getValue()));
                }
                else {
                    return new ResponseEntity<PedidoResource>(HttpStatus.IM_USED);
                }
            }
            else
                if (body.getPath().equals(PatchPath.BORDA)) {
                    if (body.getOp().equals(Patch.Operation.REPLACE)) {
                        service.atualizarBordaPedido(id, new Integer(body.getValue()));
                    }
                    else {
                        return new ResponseEntity<PedidoResource>(HttpStatus.IM_USED);
                    }
                }
                else
                    if (body.getPath().equals(PatchPath.TAMANHO)) {
                        if (body.getOp().equals(Patch.Operation.REPLACE)) {
                            service.atualizarTamanhoPedido(id, new Integer(body.getValue()));
                        }
                        else {
                            return new ResponseEntity<PedidoResource>(HttpStatus.IM_USED);
                        }
                    }
                    else
                        if (body.getPath().equals(PatchPath.QUANTIDADE)) {
                            if (body.getOp().equals(Patch.Operation.REPLACE)) {
                                service.atualizarQuantidadePedido(id, new Integer(body.getValue()));
                            }
                            else {
                                return new ResponseEntity<PedidoResource>(HttpStatus.IM_USED);
                            }
                        }
                        else {
                            return new ResponseEntity<PedidoResource>(HttpStatus.I_AM_A_TEAPOT);
                        }
        }

        Pedido pedido = service.obterPedido(id);
        PedidoResource resource = resourceAssembler.toResource(pedido);

        return new ResponseEntity<PedidoResource>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, value = Request.BORDA)
    public ResponseEntity<BordaResource> borda(@PathVariable("id") int id) {
        Pedido pedido = service.obterPedido(id);
        Borda borda = pedido.getBorda();
        BordaResource resource = bordaResourceAssembler.toResource(borda);
        resource.add(linkTo(methodOn(PedidoController.class).borda(id, borda.getId())).withRel(LinkRelationName.PEDIDO_BORDA));
        resource.add(linkTo(methodOn(PedidoController.class).pedido(id)).withRel(LinkRelationName.PEDIDO));

        return new ResponseEntity<BordaResource>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, value = Request.BORDA_ID)
    public ResponseEntity<BordaResource> borda(@PathVariable("id") int id, @PathVariable("idBorda") int idBorda) {
        Borda borda = service.obterPedido(id).getBorda();
        BordaResource resource = bordaResourceAssembler.toResource(borda);
        resource.add(linkTo(methodOn(PedidoController.class).borda(id, borda.getId())).withRel(LinkRelationName.PEDIDO_BORDA));
        resource.add(linkTo(methodOn(PedidoController.class).pedido(id)).withRel(LinkRelationName.PEDIDO));

        return new ResponseEntity<BordaResource>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, value = Request.COBERTURA)
    public ResponseEntity<List<CoberturaResource>> coberturas(@PathVariable("id") int id) {
        Pedido pedido = service.obterPedido(id);
        List<Cobertura> coberturas = pedido.getCoberturas();
        List<CoberturaResource> resource = coberturaResourceAssembler.toResourcesForPedido(id, coberturas);

        return new ResponseEntity<List<CoberturaResource>>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, value = Request.COBERTURA_ID)
    public ResponseEntity<CoberturaResource> cobertura(@PathVariable("id") int id, @PathVariable("idCobertura") int idCobertura) {
        Cobertura cobertura = service.obterCoberturaPedido(id, idCobertura);
        CoberturaResource resource = new CoberturaResource(cobertura);
        resource.add(linkTo(methodOn(PedidoController.class).cobertura(id, cobertura.getId())).withRel(LinkRelationName.SELF));
        resource.add(linkTo(methodOn(PedidoController.class).pedido(id)).withRel(LinkRelationName.PEDIDO));
        resource.add(linkTo(methodOn(CoberturaController.class).cobertura(idCobertura)).withRel(LinkRelationName.COBERTURA));

        return new ResponseEntity<CoberturaResource>(resource, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.POST, value = Request.COBERTURA_ID)
    public ResponseEntity<PedidoResource> coberturaAdicionar(@PathVariable("id") int id, @PathVariable("idCobertura") int idCobertura) {
        Pedido pedido = service.adicionarCoberturaPedido(id, idCobertura);
        PedidoResource resource = resourceAssembler.toResource(pedido);

        return new ResponseEntity<PedidoResource>(resource, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.DELETE, produces = { MediaType.APPLICATION_JSON_VALUE }, value = Request.COBERTURA_ID)
    public ResponseEntity<PedidoResource> coberturaRemover(@PathVariable("id") int id, @PathVariable("idCobertura") int idCobertura) {
        Pedido pedido = service.removerCoberturaPedido(id, idCobertura);
        PedidoResource resource = resourceAssembler.toResource(pedido);

        return new ResponseEntity<PedidoResource>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, value = Request.TAMANHO)
    public ResponseEntity<TamanhoResource> tamanho(@PathVariable("id") int id) {
        Pedido pedido = service.obterPedido(id);
        Tamanho tamanho = pedido.getTamanho();
        TamanhoResource resource = tamanhoResourceAssembler.toResource(tamanho);
        resource.add(linkTo(methodOn(PedidoController.class).tamanho(id, tamanho.getId())).withRel(LinkRelationName.PEDIDO_TAMANHO));
        resource.add(linkTo(methodOn(PedidoController.class).pedido(id)).withRel(LinkRelationName.PEDIDO));

        return new ResponseEntity<TamanhoResource>(resource, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, value = Request.TAMANHO_ID)
    public ResponseEntity<TamanhoResource> tamanho(@PathVariable("id") int id, @PathVariable("idTamanho") int idTamanho) {
        Tamanho tamanho = service.obterPedido(id).getTamanho();
        TamanhoResource resource = tamanhoResourceAssembler.toResource(tamanho);
        resource.add(linkTo(methodOn(PedidoController.class).tamanho(id, tamanho.getId())).withRel(LinkRelationName.PEDIDO_TAMANHO));
        resource.add(linkTo(methodOn(PedidoController.class).pedido(id)).withRel(LinkRelationName.PEDIDO));

        return new ResponseEntity<TamanhoResource>(resource, HttpStatus.OK);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseEntity<MessageResource> handleExceptions(Exception ex) {
        MessageResource resource = messageResourceAssembler.toResource(2, ex.getMessage());

        return new ResponseEntity<MessageResource>(resource, HttpStatus.BAD_REQUEST);
    }

}
