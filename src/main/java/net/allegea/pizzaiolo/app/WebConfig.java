package net.allegea.pizzaiolo.app;

import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import net.eusashead.hateoas.response.argumentresolver.ResponseBuilderMethodArgumentResolver;

@Configuration
public class WebConfig extends WebMvcConfigurationSupport {

    @Override
    protected void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
            argumentResolvers.add(new ResponseBuilderMethodArgumentResolver());
            super.addArgumentResolvers(argumentResolvers);
    }
    
}
