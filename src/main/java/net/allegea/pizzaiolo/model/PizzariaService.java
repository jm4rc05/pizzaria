package net.allegea.pizzaiolo.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class PizzariaService {

    private Map<Integer, Massa> massas = new HashMap<Integer, Massa>();

    private int _idMassa = 0;

    private Map<Integer, Borda> bordas = new HashMap<Integer, Borda>();

    private int _idBorda = 0;

    private Map<Integer, Tamanho> tamanhos = new HashMap<Integer, Tamanho>();

    private int _idTamanho = 0;

    private Map<Integer, Cobertura> coberturas = new HashMap<Integer, Cobertura>();

    private int _idCobertura = 0;

    private Map<Integer, Cardapio> cardapios = new HashMap<Integer, Cardapio>();

    private int _idCardapio = 0;

    private Map<Integer, Pedido> pedidos = new HashMap<Integer, Pedido>();

    private int _idPedido = 0;

    private Map<Integer, Message> messages = new HashMap<Integer, Message>();

    public PizzariaService() {
        adicionarMassa(new Massa("Fina", 12)); // 1
        adicionarMassa(new Massa("Media", 15)); // 2
        adicionarMassa(new Massa("Grossa", 19)); // 3

        adicionarBorda(new Borda("Sem recheio", 0)); // 1
        adicionarBorda(new Borda("Recheada de catupiry", 10)); // 2
        adicionarBorda(new Borda("Recheada de cheddar", 12)); // 3

        adicionarTamanho(new Tamanho("Aperitivo", 1)); // 1
        adicionarTamanho(new Tamanho("Brotinho", 2)); // 2
        adicionarTamanho(new Tamanho("Grande", 6)); // 3
        adicionarTamanho(new Tamanho("Familia", 10)); // 4
        adicionarTamanho(new Tamanho("Gigante", 12)); // 5

        adicionarCobertura(new Cobertura("Mozzarella", 5)); // 1
        adicionarCobertura(new Cobertura("Pepperoni", 7)); // 2
        adicionarCobertura(new Cobertura("Salame", 6)); // 3
        adicionarCobertura(new Cobertura("Azeitona", 5)); // 4
        adicionarCobertura(new Cobertura("Palmito", 6)); // 5
        adicionarCobertura(new Cobertura("Atum", 5)); // 6
        adicionarCobertura(new Cobertura("Cebola", 3)); // 7
        adicionarCobertura(new Cobertura("Tomate", 4)); // 8
        adicionarCobertura(new Cobertura("Abobrinha", 4)); // 9
        adicionarCobertura(new Cobertura("Rucula", 4)); // 10
        adicionarCobertura(new Cobertura("Tomate seco", 5)); // 11
        adicionarCobertura(new Cobertura("Pimentao", 4)); // 12
        adicionarCobertura(new Cobertura("Cogumelo", 6)); // 13
        adicionarCobertura(new Cobertura("Manjericao", 3)); // 14
        adicionarCobertura(new Cobertura("Calabresa", 6)); // 15

        Message message1 = new Message(1, Message.Type.INFO, "001", "Ok", "Everything is all right!");
        messages.put(1, message1); // 1
        Message message2 = new Message(2, Message.Type.ERROR, "999", "Error", "Something went very badly");
        messages.put(2, message2); // 2

        List<Cobertura> receita;
        receita = new ArrayList<Cobertura>();
        receita.add(coberturas.get(1));
        receita.add(coberturas.get(14));
        receita.add(coberturas.get(8));
        adicionarCardapio(new Cardapio("Marguerita", massas.get(0), bordas.get(0), receita)); // 1
        receita = new ArrayList<Cobertura>();
        receita.add(coberturas.get(1));
        receita.add(coberturas.get(6));
        receita.add(coberturas.get(7));
        adicionarCardapio(new Cardapio("Atum", massas.get(0), bordas.get(0), receita)); // 2
        receita = new ArrayList<Cobertura>();
        receita.add(coberturas.get(1));
        receita.add(coberturas.get(15));
        adicionarCardapio(new Cardapio("Calabresa", massas.get(0), bordas.get(0), receita)); // 3
    }

    public List<Pedido> obterPedidos() {
        return new ArrayList<Pedido>(pedidos.values());
    }

    public Pedido obterPedido(int id) {
        return pedidos.get(id);
    }

    public Pedido adicionarPedido(PedidoRequest pedidoRequest) {
        Pedido pedidoNovo = new Pedido();

        pedidoNovo.setCliente(pedidoRequest.getCliente());
        pedidoNovo.setEndereco(pedidoRequest.getEndereco());
        pedidoNovo.setPizza(cardapios.get(pedidoRequest.getIdPizza()));
        pedidoNovo.setMassa(massas.get(pedidoRequest.getIdMassa()));
        pedidoNovo.setBorda(bordas.get(pedidoRequest.getIdBorda()));
        pedidoNovo.setTamanho(tamanhos.get(pedidoRequest.getIdTamanho()));
        pedidoNovo.setQuantidade(pedidoRequest.getQuantidade());
        pedidoNovo.setId(++_idPedido);

        pedidos.put(pedidoNovo.getId(), pedidoNovo);

        return pedidoNovo;
    }

    public Pedido atualizarPedido(int id, PedidoRequest pedidoRequest) {
        Pedido pedidoAtual = pedidos.get(id);

        pedidoAtual.setCliente(pedidoRequest.getCliente());
        pedidoAtual.setEndereco(pedidoRequest.getEndereco());
        pedidoAtual.setPizza(cardapios.get(pedidoRequest.getIdPizza()));
        pedidoAtual.setMassa(massas.get(pedidoRequest.getIdMassa()));
        pedidoAtual.setBorda(bordas.get(pedidoRequest.getIdBorda()));
        pedidoAtual.setTamanho(tamanhos.get(pedidoRequest.getIdTamanho()));
        pedidoAtual.setQuantidade(pedidoRequest.getQuantidade());

        pedidos.put(id, pedidoAtual);

        return pedidoAtual;
    }

    public Pedido removerPedido(int id) {
        Pedido pedido = obterPedido(id);
        pedidos.remove(id);

        return pedido;
    }

    public List<Borda> obterBordas() {
        return new ArrayList<Borda>(bordas.values());
    }

    public Borda obterBorda(int id) {
        return bordas.get(id);
    }

    public Borda adicionarBorda(Borda borda) {
        borda.setId(++_idBorda);

        bordas.put(_idBorda, borda);

        return borda;
    }

    public Borda atualizarBorda(int id, Borda borda) {
        bordas.put(id, borda);

        return borda;
    }

    public Borda removerBorda(String nome) {
        Borda borda = bordas.get(nome);

        bordas.remove(nome);

        return borda;
    }

    public Pedido atualizarBordaPedido(int id, int idBorda) {
        Pedido pedido = pedidos.get(id);
        Borda borda = bordas.get(idBorda);

        pedido.setBorda(borda);

        pedidos.put(id, pedido);

        return pedido;
    }

    public List<Massa> obterMassas() {
        return new ArrayList<Massa>(massas.values());
    }

    public Massa obterMassa(int id) {
        return massas.get(id);
    }

    public Massa adicionarMassa(Massa massa) {
        massa.setId(++_idMassa);

        massas.put(_idMassa, massa);

        return massa;
    }

    public Massa atualizarMassa(int id, Massa massa) {
        massas.put(id, massa);

        return massa;
    }

    public Massa removerMassa(String nome) {
        Massa massa = massas.get(nome);

        massas.remove(nome);

        return massa;
    }

    public Pedido atualizarMassaPedido(int id, int idMassa) {
        Pedido pedido = pedidos.get(id);
        Massa massa = massas.get(idMassa);

        pedido.setMassa(massa);

        pedidos.put(id, pedido);

        return pedido;
    }

    public List<Cobertura> obterCoberturas() {
        return new ArrayList<Cobertura>(coberturas.values());
    }

    public Cobertura obterCoberturaPedido(int id, int idCobertura) {
        Pedido pedido = pedidos.get(id);
        Cobertura coberturaPedido = null;
        for (Cobertura cobertura : pedido.getCoberturas()) {
            if (cobertura.getId().equals(idCobertura)) {
                coberturaPedido = cobertura;
                break;
            }
        }

        return coberturaPedido;
    }

    public Cobertura obterCoberturaCardapio(int id, int idCobertura) {
        Cardapio cardapio = cardapios.get(id);
        Cobertura coberturaCardapio = null;
        for (Cobertura cobertura : cardapio.getCoberturas()) {
            if (cobertura.getId().equals(idCobertura)) {
                coberturaCardapio = cobertura;
                break;
            }
        }

        return coberturaCardapio;
    }

    public Cobertura obterCobertura(int id) {
        return coberturas.get(id);
    }

    public Cobertura adicionarCobertura(Cobertura cobertura) {
        cobertura.setId(++_idCobertura);

        coberturas.put(_idCobertura, cobertura);

        return cobertura;
    }

    public Cobertura atualizarCobertura(int id, Cobertura cobertura) {
        coberturas.put(id, cobertura);

        return cobertura;
    }

    public Cobertura removerCobertura(String nome) {
        Cobertura cobertura = coberturas.get(nome);

        coberturas.remove(nome);

        return cobertura;
    }

    public Pedido adicionarCoberturaPedido(int id, int idCobertura) {
        Pedido pedido = pedidos.get(id);
        Cobertura cobertura = coberturas.get(idCobertura);

        List<Cobertura> coberturas = pedido.getCoberturas();
        coberturas.add(cobertura);
        pedido.setCoberturas(coberturas);
        pedidos.put(id, pedido);

        return pedido;
    }

    public Pedido removerCoberturaPedido(int id, int idCobertura) {
        Pedido pedido = pedidos.get(id);
        Cobertura cobertura = coberturas.get(idCobertura);

        List<Cobertura> coberturas = pedido.getCoberturas();
        coberturas.remove(cobertura);
        pedido.setCoberturas(coberturas);
        pedidos.put(id, pedido);

        return pedido;
    }

    public Cardapio adicionarCoberturaCardapio(int id, int idCobertura) {
        Cardapio cardapio = cardapios.get(id);
        Cobertura cobertura = coberturas.get(idCobertura);

        List<Cobertura> coberturas = cardapio.getCoberturas();
        coberturas.add(cobertura);
        cardapio.setCoberturas(coberturas);
        cardapios.put(id, cardapio);

        return cardapio;
    }

    public Cardapio removerCoberturaCardapio(int id, int idCobertura) {
        Cardapio cardapio = cardapios.get(id);
        Cobertura cobertura = coberturas.get(idCobertura);

        List<Cobertura> coberturas = cardapio.getCoberturas();
        coberturas.remove(cobertura);
        cardapio.setCoberturas(coberturas);
        cardapios.put(id, cardapio);

        return cardapio;
    }

    public List<Tamanho> obterTamanhos() {
        return new ArrayList<Tamanho>(tamanhos.values());
    }

    public Tamanho obterTamanho(int id) {
        return tamanhos.get(id);
    }

    public Tamanho adicionarTamanho(Tamanho tamanho) {
        tamanho.setId(++_idTamanho);

        tamanhos.put(_idTamanho, tamanho);

        return tamanho;
    }

    public Tamanho atualizarTamanho(int id, Tamanho tamanho) {
        tamanhos.put(id, tamanho);

        return tamanho;
    }

    public Tamanho removerTamanho(String nome) {
        Tamanho tamanho = tamanhos.get(nome);

        tamanhos.remove(nome);

        return tamanho;
    }

    public Pedido atualizarTamanhoPedido(int id, int idTamanho) {
        Pedido pedido = pedidos.get(id);
        Tamanho tamanho = tamanhos.get(idTamanho);

        pedido.setTamanho(tamanho);

        pedidos.put(id, pedido);

        return pedido;
    }

    public Pedido atualizarQuantidadePedido(int id, int quantidade) {
        Pedido pedido = pedidos.get(id);

        pedido.setQuantidade(quantidade);

        pedidos.put(id, pedido);

        return pedido;
    }

    public List<Cardapio> obterCardapios() {
        return new ArrayList<Cardapio>(cardapios.values());
    }

    public Cardapio obterCardapio(int id) {
        return cardapios.get(id);
    }

    public Cardapio adicionarCardapio(Cardapio cardapio) {
        cardapio.setId(++_idCardapio);

        cardapios.put(_idCardapio, cardapio);

        return cardapio;
    }

    public Cardapio atualizarCardapio(int id, Cardapio cardapio) {
        cardapios.put(id, cardapio);

        return cardapio;
    }

    public Cardapio removerCardapio(String nome) {
        Cardapio cardapio = cardapios.get(nome);

        cardapios.remove(nome);

        return cardapio;
    }

    public Message obterMessage(int id) {
        Message message = messages.get(id);

        return message;
    }

    public Message obterMessageInfo(int id, String info) {
        Message message = messages.get(id);
        Message resourceMessage = new Message(message.getId(), message.getType(), message.getStatus(), message.getDescription(), info);

        return resourceMessage;
    }

    public List<Message> obterMessages() {
        return new ArrayList<Message>(messages.values());
    }

}
