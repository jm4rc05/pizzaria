package net.allegea.pizzaiolo.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.Identifiable;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Pedido implements Serializable, Identifiable<Integer> {

    private static final long serialVersionUID = 1L;

    @JsonIgnore
    private int id;

    private String cliente;

    private String endereco;

    private String pizza;

    private Massa massa;

    private Borda borda;

    private List<Cobertura> coberturas;

    private Tamanho tamanho;

    private int quantidade;

    public Pedido() {
        super();

        this.cliente = "";
        this.endereco = "";
        this.pizza = "";
        this.massa = new Massa();
        this.borda = new Borda();
        this.coberturas = new ArrayList<Cobertura>();
        this.tamanho = new Tamanho();
        this.quantidade = 0;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    @JsonIgnore
    public void setPizza(Cardapio pizza) {
        this.pizza = pizza.getNome();
        this.coberturas = new ArrayList<Cobertura>(pizza.getCoberturas());
    }

    public String getPizza() {
        return pizza;
    }

    public void setPizza(String pizza) {
        this.pizza = pizza;
    }

    public Massa getMassa() {
        return massa;
    }

    public void setMassa(Massa massa) {
        this.massa = massa;
    }

    public Borda getBorda() {
        return borda;
    }

    public void setBorda(Borda borda) {
        this.borda = borda;
    }

    public List<Cobertura> getCoberturas() {
        return coberturas;
    }

    public void setCoberturas(List<Cobertura> coberturas) {
        this.coberturas = coberturas;
    }

    public Tamanho getTamanho() {
        return tamanho;
    }

    public void setTamanho(Tamanho tamanho) {
        this.tamanho = tamanho;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getValor() {
        double valorTotal = 0;
        for (Cobertura cobertura : coberturas) {
            valorTotal += cobertura.getValor();
        }
        valorTotal += massa.getValor();
        valorTotal += borda.getValor();
        valorTotal *= quantidade;
        valorTotal *= tamanho.getPedacos();
        valorTotal /= 10;

        return valorTotal;
    }

}
