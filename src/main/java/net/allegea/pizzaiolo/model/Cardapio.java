package net.allegea.pizzaiolo.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.Identifiable;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Cardapio implements Serializable, Identifiable<Integer> {

    private static final long serialVersionUID = 1L;

    private int id;

    private String nome;

    private List<Cobertura> coberturas;

    public Cardapio() {
        super();

        this.nome = "";
        this.setCoberturas(new ArrayList<Cobertura>());
    }

    public Cardapio(String nome, Massa massa, Borda borda, List<Cobertura> coberturas) {
        super();

        this.nome = nome;
        this.setCoberturas(coberturas);
    }

    @JsonIgnore
    public Integer getId() {
        return id;
    }

    @JsonIgnore
    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Cobertura> getCoberturas() {
        return coberturas;
    }

    public void setCoberturas(List<Cobertura> coberturas) {
        this.coberturas = new ArrayList<Cobertura>(coberturas);
    }

}
