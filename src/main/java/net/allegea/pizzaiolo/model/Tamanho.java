package net.allegea.pizzaiolo.model;

import java.io.Serializable;

import org.springframework.hateoas.Identifiable;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Tamanho implements Serializable, Identifiable<Integer> {

    private static final long serialVersionUID = 1L;

    private int id;

    private String nome;

    private int pedacos;

    public Tamanho() {
        super();

        this.nome = "";
        this.pedacos = 0;
    }

    public Tamanho(String nome, int pedacos) {
        super();

        this.nome = nome;
        this.pedacos = pedacos;
    }

    @JsonIgnore
    public Integer getId() {
        return id;
    }

    @JsonIgnore
    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getPedacos() {
        return pedacos;
    }

    public void setPedacos(int pedacos) {
        this.pedacos = pedacos;
    }

}
