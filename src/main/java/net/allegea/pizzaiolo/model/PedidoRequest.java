package net.allegea.pizzaiolo.model;

import java.io.Serializable;

public class PedidoRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    private String cliente;

    private String endereco;

    private int idPizza;

    private int idMassa;

    private int idBorda;

    private int idTamanho;

    private int quantidade;

    public PedidoRequest() {
        super();

        this.cliente = "";
        this.endereco = "";
        this.idPizza = 0;
        this.idMassa = 0;
        this.idBorda = 0;
        this.idTamanho = 0;
        this.quantidade = 0;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public int getIdPizza() {
        return idPizza;
    }

    public void setIdPizza(int idPizza) {
        this.idPizza = idPizza;
    }

    public int getIdMassa() {
        return idMassa;
    }

    public void setIdMassa(int idMassa) {
        this.idMassa = idMassa;
    }

    public int getIdBorda() {
        return idBorda;
    }

    public void setIdBorda(int idBorda) {
        this.idBorda = idBorda;
    }

    public int getIdTamanho() {
        return idTamanho;
    }

    public void setIdTamanho(int idTamanho) {
        this.idTamanho = idTamanho;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

}
