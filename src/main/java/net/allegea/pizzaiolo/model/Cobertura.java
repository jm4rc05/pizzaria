package net.allegea.pizzaiolo.model;

import java.io.Serializable;

import org.springframework.hateoas.Identifiable;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Cobertura implements Serializable, Identifiable<Integer> {

    private static final long serialVersionUID = 1L;

    private int id;

    private String nome;

    private double valor;

    public Cobertura() {
        super();

        this.nome = "";
        this.valor = 0;
    }

    public Cobertura(String nome, double valor) {
        super();

        this.nome = nome;
        this.valor = valor;
    }

    @JsonIgnore
    public Integer getId() {
        return id;
    }

    @JsonIgnore
    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

}
